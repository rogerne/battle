Given("I am on the home page having created a game") do
  visit root_path
  click_on 'New Game'
  fill_in('Pa name', :with => 'John')
  fill_in('Pb name', :with => 'Doe')
  sleep(2)
  click_on 'Create Game'
  sleep(2)
  click_on 'Back'
end

When("I click on the Play link") do
  click_on 'Play'
  sleep(2)
end

Then("I should be taken to the deploy page") do
  expect(page).to have_text('Deploy your Aircraft Carrier')
  expect(page).to have_content('Player A = John | Player B = Doe' || 'Player A = Doe | Player B = John')
end