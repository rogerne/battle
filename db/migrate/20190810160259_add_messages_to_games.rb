class AddMessagesToGames < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :message, :string
    add_column :games, :score, :text
  end
end
