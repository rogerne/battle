class AddHistoriesToGames < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :p1_dg_history, :text
    add_column :games, :p2_dg_history, :text
  end
end
