class AddCanSaveToGames < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :can_save, :boolean
  end
end
