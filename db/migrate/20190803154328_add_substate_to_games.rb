class AddSubstateToGames < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :sub_status, :string
  end
end
