class AddTwoMoreGridsToGames < ActiveRecord::Migration[5.2]
  def change
  	add_column :games, :p2_display_grid, :text
    add_column :games, :p2_game_grid, :text
  end
end
