class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :p1_name
      t.string :p2_name
      t.text :visible_grid
      t.text :hashed_grid
      t.string :direction
      t.string :x_pos
      t.string :y_pos

      t.timestamps
    end
  end
end
