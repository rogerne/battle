class ChangeVisibleGridName < ActiveRecord::Migration[5.2]
  def change
  	rename_column :games, :visible_grid, :p1_hashed_display_grid
  	rename_column :games, :hashed_grid, :p1_hashed_ocean_grid
  	rename_column :games, :targ_visible_grid, :p1_display_grid
  	rename_column :games, :targ_hashed_grid, :p1_ocean_grid
  	rename_column :games, :p2_visible_grid, :p1_game_grid
  end
end
