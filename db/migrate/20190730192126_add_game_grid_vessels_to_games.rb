class AddGameGridVesselsToGames < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :p1_game_grid_vessels, :text
    add_column :games, :p2_game_grid_vessels, :text
  end
end
