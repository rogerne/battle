class AddDeployCountToGames < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :deploy_count, :string
  end
end
