class ChangePlayerNames < ActiveRecord::Migration[5.2]
  def change
  	rename_column :games, :p1_name, :pA_name
  	rename_column :games, :p2_name, :pB_name
  end
end
