class AddGridsToGames < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :targ_visible_grid, :text
    add_column :games, :targ_hashed_grid, :text
    add_column :games, :p2_ocean_grid, :text
    add_column :games, :p2_visible_grid, :text
  end
end
