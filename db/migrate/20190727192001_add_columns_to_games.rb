class AddColumnsToGames < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :p2_hashed_display_grid, :text
    add_column :games, :p2_hashed_ocean_grid, :text
    add_column :games, :p2_hashed_game_grid, :text
  end
end
