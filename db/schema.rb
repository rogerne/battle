# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_10_160259) do

  create_table "games", force: :cascade do |t|
    t.string "pA_name"
    t.string "pB_name"
    t.text "p1_hashed_display_grid"
    t.text "p1_hashed_ocean_grid"
    t.string "direction"
    t.string "x_pos"
    t.string "y_pos"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "deploy_count"
    t.string "state"
    t.boolean "can_save"
    t.text "p1_display_grid"
    t.text "p1_ocean_grid"
    t.text "p2_ocean_grid"
    t.text "p1_game_grid"
    t.text "p2_display_grid"
    t.text "p2_game_grid"
    t.text "p1_hashed_game_grid"
    t.text "p2_hashed_display_grid"
    t.text "p2_hashed_ocean_grid"
    t.text "p2_hashed_game_grid"
    t.string "current_player"
    t.text "p1_game_grid_vessels"
    t.text "p2_game_grid_vessels"
    t.string "sub_status"
    t.text "p1_dg_history"
    t.text "p2_dg_history"
    t.string "message"
    t.text "score"
  end

end
