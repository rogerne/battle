require 'test_helper'

class GamesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @game = games(:one)
  end

  test "should get index" do
    get games_url
    assert_response :success
  end

  test "should get new" do
    get new_game_url
    assert_response :success
  end

  test "should create game" do
    assert_difference('Game.count') do
      post games_url, params: { game: { direction: @game.direction, hashed_grid: @game.hashed_grid, pA_name: @game.p1_name, pB_name: @game.p2_name, visible_grid: @game.visible_grid, x_pos: @game.x_pos, y_pos: @game.y_pos } }
    end

    assert_redirected_to game_url(Game.last)
  end

  test "should show game" do
    get game_url(@game)
    assert_response :success
  end

  test "should get edit" do
    get edit_game_url(@game)
    assert_response :success
  end

  test "should update game" do
    patch game_url(@game), params: { game: { direction: @game.direction, hashed_grid: @game.hashed_grid, pA_name: @game.p1_name, pB_name: @game.p2_name, visible_grid: @game.visible_grid, x_pos: @game.x_pos, y_pos: @game.y_pos } }
    assert_redirected_to game_url(@game)
  end

  test "should destroy game" do
    assert_difference('Game.count', -1) do
      delete game_url(@game)
    end

    assert_redirected_to games_url
  end
end
