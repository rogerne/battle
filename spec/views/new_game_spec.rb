require 'rails_helper'
 
RSpec.describe 'New game', type: :feature do
  scenario 'index page' do
    visit root_path
    #sleep(1)
    click_on 'New Game'
    expect(page).to have_content('New Game')
    #sleep(1)
  end

  scenario 'index page' do
    visit root_path
    sleep(3)
    click_on 'New Game'
    fill_in('Pa name', :with => 'John')
    fill_in('Pb name', :with => 'Doe')
    sleep(3)
    click_on 'Create Game'
    expect(page).to have_content('Deploy your Aircraft Carrier')
    expect(page).to have_content('Player A = John').or have_content('Player A = Doe')
    expect(page).to have_content('Player B = John').or have_content('Player B = Doe')
    sleep(1)
  end

end
