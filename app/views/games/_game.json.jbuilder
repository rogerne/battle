json.extract! game, :id, :p1_name, :p2_name, :visible_grid, :hashed_grid, :direction, :x_pos, :y_pos, :created_at, :updated_at
json.url game_url(game, format: :json)
