class Game < ApplicationRecord
  after_initialize :init
  serialize :p1_hashed_display_grid, Hash
  serialize :p1_hashed_ocean_grid, Hash 
  serialize :p1_hashed_game_grid, Hash 
  serialize :p2_hashed_display_grid, Hash
  serialize :p2_hashed_ocean_grid, Hash 
  serialize :p2_hashed_game_grid, Hash 
  serialize :p1_dg_history, Hash
  serialize :p2_dg_history, Hash
  serialize :score, Hash

  serialize :p1_display_grid, Array
  serialize :p1_ocean_grid, Array
  serialize :p1_game_grid, Array
  serialize :p2_display_grid, Array
  serialize :p2_ocean_grid, Array
  serialize :p2_game_grid, Array
  serialize :p1_game_grid_vessels, Array
  serialize :p2_game_grid_vessels, Array 

  def init
    self.pA_name  ||= "Neil" 
    self.pB_name  ||=  "PC"
    self.current_player ||= set_initial_player
    #self.current_player ||= "play_PlayerA"
    self.state  ||= "new"
    self.deploy_count ||= 0
    self.direction ||= "H"
    self.x_pos ||= "B"   
    self.y_pos  ||= "2"
    @letters = Hash.new
    @numbers = Hash.new
    fill_letters 10
  end

  before_save() do
    unless new_record? 
      case self.state
        when "deploying"
          all_reqd_values?
          create_deploying_grids
          @p1_game_grid.replace_grid(self.p1_game_grid)
          deploy_vessel
          store_deployed_vessels
          if self.deploy_count == "5"#Before going to playing screen show the deployed fleet one more time
              self.state = "deployed"
          end
          self.p1_hashed_game_grid = @p1_game_grid.grid_container
          self.direction = "H"
          self.x_pos = "D"   
          self.y_pos = "2"
        when "deployed" #Before going to playing screen set up all the game playing grids
          puts "In deployed"
          create_deploying_grids
          
          @p1_game_grid.replace_grid(self.p1_game_grid)

          auto_deploy_nonHumanPlayer
          self.p2_game_grid = @p2_game_grid.grid
          
          create_playing_setup_grids
          @p1_ocean_grid.replace_grid @p1_game_grid.grid
          @p2_ocean_grid.replace_grid @p2_game_grid.grid

          self.p1_hashed_display_grid = @p1_display_grid.grid_container
          self.p1_display_grid = @p1_display_grid.grid
          self.p1_hashed_ocean_grid = @p1_ocean_grid.grid_container
          self.p1_ocean_grid = @p1_ocean_grid.grid

          self.p2_hashed_display_grid = @p2_display_grid.grid_container
          self.p2_display_grid = @p2_display_grid.grid
          self.p2_hashed_ocean_grid = @p2_ocean_grid.grid_container
          self.p2_ocean_grid = @p2_ocean_grid.grid    

          store_deployed_vessels
          store_deployed_dg_history
          self.state = "playing"

          case current_player 
            when "PlayerA" #Human player to play 1st
              self.sub_status = "play_PlayerA"
            when "PlayerB" #PC to play 1st
              swap_and_store_grids
              swap_and_store_deployed_vessels
              swap_and_store_dg_history
              create_playing_grids
              play = @p1_display_grid.get_play
              result = @p2_game_grid.hit(play)
              self.message = add_message(play, result)
              #update_grids_with_result
              @p1_display_grid.update_grid(play, result)
              puts_the_history              

              self.p1_hashed_display_grid = @p1_display_grid.grid_container
              self.p1_display_grid = @p1_display_grid.grid
              self.p1_dg_history = @p1_display_grid.history
              "After play"
              puts_the_history
              self.p1_hashed_ocean_grid = @p1_ocean_grid.grid_container

              @p2_ocean_grid.update_grid(play, result)
              self.p2_hashed_ocean_grid = @p2_ocean_grid.grid_container
              self.p2_ocean_grid = @p2_ocean_grid.grid
              self.p2_hashed_display_grid = @p2_display_grid.grid_container
              #store_deployed_vessels
              self.p1_game_grid_vessels = @p1_game_grid.vessels_array
              self.p2_game_grid_vessels = @p2_game_grid.vessels_array
              
              self.sub_status = "show_PlayerB"
          end
          self.direction = ""
          self.x_pos = ""   
          self.y_pos = ""

        when "playing"  #TODO "Playing"
        case self.sub_status 
            when "show_PlayerB"
              show   
              self.sub_status = "play_PlayerA"
            when "play_PlayerA"
              do_a_play 
              self.sub_status = "show_PlayerA"
            when "show_PlayerA"
              create_playing_grids
              swap_players
              swap_and_store_deployed_vessels
              swap_and_store_dg_history
              create_playing_grids
              play = @p1_display_grid.get_play
              result = @p2_game_grid.hit(play)
              puts "result = #{result}"
              self.message = add_message(play, result)
              update_score result
              #score["PlayerB"] += 1 if result == "S"
              @p1_display_grid.update_grid(play, result)
              puts_the_history
              self.p1_hashed_display_grid = @p1_display_grid.grid_container
              self.p1_display_grid = @p1_display_grid.grid
              self.p1_dg_history = @p1_display_grid.history
              self.p1_hashed_ocean_grid = @p1_ocean_grid.grid_container
              @p2_ocean_grid.update_grid(play, result)
              self.p2_hashed_ocean_grid = @p2_ocean_grid.grid_container
              self.p2_ocean_grid = @p2_ocean_grid.grid
              self.p2_hashed_display_grid = @p2_display_grid.grid_container
              self.p1_game_grid_vessels = @p1_game_grid.vessels_array
              self.p2_game_grid_vessels = @p2_game_grid.vessels_array
              self.sub_status = "show_PlayerB"
          end
      end 
      else #When its a new record set all blank grids
        create_deploying_grids
        set_deploying_grids
        self.score = {"PlayerA" => 0, "PlayerB" => 0}
        self.state = "deploying"
    end
  end
 
private
  def add_message(play, result)
     self.current_player == "PlayerA" ? name = self.pA_name : name = self.pB_name
     msg = self.message = "#{name}'s shot landed in grid reference #{play}."
     case result
     when "H"
      msg += "The result was a hit."
     when "S"
      msg += "The vessel was sunk."
     else
      msg += "The result was a miss."
     end
     return msg
  end
  def create_playing_grids
    @p1_display_grid = DisplayGrid.new(10)
    @p1_ocean_grid = OceanGrid.new(10)
    @p1_game_grid = GameGrid.new(10)
 
    @p2_display_grid = DisplayGrid.new(10)
    @p2_ocean_grid = OceanGrid.new(10)
    @p2_game_grid = GameGrid.new(10)

    redeploy_vessels
    redeploy_history

    @p1_display_grid.replace_grid(self.p1_display_grid)
    @p1_display_grid.history = self.p1_dg_history
    @p1_ocean_grid.replace_grid(self.p1_ocean_grid)
    @p1_game_grid.replace_grid(self.p1_game_grid)
    
    @p2_display_grid.replace_grid(self.p2_display_grid)
    @p2_display_grid.history = self.p2_dg_history
    @p2_ocean_grid.replace_grid(self.p2_ocean_grid)
    @p2_game_grid.replace_grid(self.p2_game_grid)
  end

  def swap_and_store_grids
    p1_dg = self.p1_display_grid
    p1_h_dg = self.p1_hashed_display_grid
    p1_og = self.p1_ocean_grid
    p1_h_og = self.p1_hashed_ocean_grid 
    p1_gg = self.p1_game_grid

    self.p1_display_grid = self.p2_display_grid
    self.p1_hashed_display_grid = self.p2_hashed_display_grid 
    self.p1_ocean_grid = self.p2_ocean_grid
    self.p1_hashed_ocean_grid = self.p2_hashed_ocean_grid 
    self.p1_game_grid = self.p2_game_grid

    self.p2_display_grid = p1_dg
    self.p2_hashed_display_grid = p1_h_dg 
    self.p2_ocean_grid = p1_og
    self.p2_hashed_ocean_grid = p1_h_og 
    self.p2_game_grid = p1_gg
  end

  def swap_and_store_deployed_vessels
    self.p1_game_grid_vessels = @p2_game_grid.vessels_array
    self.p2_game_grid_vessels = @p1_game_grid.vessels_array
  end

  def swap_and_store_dg_history
    puts "before swap_and_store_dg_history" 
    puts_the_history
    self.p1_dg_history = @p2_display_grid.history
    self.p2_dg_history = @p1_display_grid.history
    puts "after swap_and_store_dg_history" 
    puts_the_history
  end

  def redeploy_vessels
    self.p1_game_grid_vessels.each do |vs|
      v = Vessel.new(vs["type"],vs["direction"],vs["x"],vs["y"],vs["hits"],vs["sunk"]) 
      @p1_game_grid.add_vessel(v)
    end
    self.p2_game_grid_vessels.each do |vs|
      v = Vessel.new(vs["type"],vs["direction"],vs["x"],vs["y"],vs["hits"],vs["sunk"]) 
      @p2_game_grid.add_vessel(v)
    end
  end

  def redeploy_history
    puts "Redeploying history"
    @p1_display_grid.history = self.p1_dg_history
    @p2_display_grid.history = self.p2_dg_history
    puts_the_history
  end

  def do_a_play
    create_playing_grids
    case current_player
      when "PlayerA"
        #puts "In PlayerA"
        check_all_playing_values?
        play = self.x_pos + self.y_pos
        #puts "play = #{play}"
        if @p1_display_grid.is_valid_ref?(play) && @p1_display_grid.can_take_hit?(play)
            make_a_play(play)
          else
            errors.add(:base, "Please choose a valid Grid Position")
            throw :abort
        end
        self.sub_status = "show_PlayerA"
      when "PlayerB"
    end
      self.x_pos = ""   
      self.y_pos = ""
  end

  def make_a_play(play)
    result = @p2_game_grid.hit(play)
    puts "result = #{result}"
    self.message = add_message(play, result)
    #score["PlayerA"] += 1 if result == "S"
    update_score result
    update_grids_with_result play, result
  end

  def update_score(result)
    puts "update_score(result)"
    score[self.current_player] += 1 if result == "S"
    #self.state = "gameover" if score[current_player] == 5
    puts  "score[self.current_player] = #{score[self.current_player]} "
    if score[current_player].to_i == 5
      puts "In here"
      self.current_player == "PlayerA" ? name = self.pA_name : name = self.pB_name
      self.message = "#{self.message} Congratulations! #{name}  is the winner."
      self.state = "gameover" 
    end
  end

  def update_grids_with_result(play, result)
    @p1_display_grid.update_grid(play, result)
    puts "update_grids_with_result"
    puts_the_history
    self.p1_hashed_display_grid = @p1_display_grid.grid_container
    self.p1_display_grid = @p1_display_grid.grid
    self.p1_dg_history = @p1_display_grid.history
    self.p1_hashed_ocean_grid = @p1_ocean_grid.grid_container

    @p2_ocean_grid.update_grid(play, result)
    self.p2_hashed_ocean_grid = @p2_ocean_grid.grid_container
    self.p2_ocean_grid = @p2_ocean_grid.grid
    self.p2_hashed_display_grid = @p2_display_grid.grid_container

    store_deployed_vessels 
  end

  def show
    create_playing_grids
    swap_players
    swap_and_store_deployed_vessels
    swap_and_store_dg_history
    #puts "Swapped Grids"
    self.sub_status = "show"
    self.x_pos = ""   
    self.y_pos = ""
  end

  def swap_players
    swap_and_store_grids
    if self.current_player == "PlayerA"
        self.current_player  = "PlayerB"
      else
        self.current_player  = "PlayerA"
      end    
  end

  def set_initial_player
    players = ["PlayerA", "PlayerB"]
    return players[rand(0..1)]
  end

  def store_deployed_vessels
    self.p1_game_grid_vessels = @p1_game_grid.vessels_array
    self.p2_game_grid_vessels = @p2_game_grid.vessels_array
  end

  def store_deployed_dg_history
    self.p1_dg_history = @p1_display_grid.history
    self.p2_dg_history = @p2_display_grid.history
    puts_the_history
  end

  def puts_the_vessels
    puts "p1_game_grid_vessels"
    puts self.p1_game_grid_vessels
    puts "p2_game_grid_vessels"
    puts self.p2_game_grid_vessels
  end
  
  def puts_the_history
    puts "@p1_display_grid history"
    puts @p1_display_grid.history
    puts "@p2_display_grid history"
    puts @p2_display_grid.history
    puts "self.p1_dg_history"
    puts self.p1_dg_history
    puts "self.p2_dg_history"
    puts self.p2_dg_history
  end

  def create_deploying_grids
    @p1_game_grid = GameGrid.new(10)
    @p2_game_grid = GameGrid.new(10)

    redeploy_vessels
  end

  def create_playing_setup_grids
    @p1_display_grid = DisplayGrid.new(10)
    @p1_ocean_grid = OceanGrid.new(10)
 
    @p2_display_grid = DisplayGrid.new(10)
    @p2_ocean_grid = OceanGrid.new(10)
  end

  def set_deploying_grids
    self.p1_game_grid = @p1_game_grid.grid
    self.p1_hashed_game_grid = @p1_game_grid.grid_container
  end
 
  def auto_deploy_nonHumanPlayer
    @fleet = {"AC" => "Aircraft Carrier", "BA" => "Battleship" , "CR" => "Cruiser", "SU" => "Submarine", "DE" => "Destroyer"}
    @p2_game_grid.auto_deploy(@fleet)
  end
 
  def deploy_vessel
    @fleet = { "0" => "AC", "1" => "BA" , "2" => "CR", "3" => "SU", "4" => "DE"}
    new_vessel = Vessel.new @fleet[self.deploy_count] , self.direction, self.x_pos , self.y_pos.to_i
    can_deploy_vessel?(new_vessel)
    @p1_game_grid.add_vessel new_vessel
    self.deploy_count = self.deploy_count.to_i + 1
  end
 
  def can_deploy_vessel?(vessel)
    unless @p1_game_grid.can_add_vessel?(vessel)
        errors.add(:base, "Ship will not fit or will lie on top of an existing one.")
        throw :abort
    end
  end
 
  def all_reqd_values?
    if self.direction == '' || self.x_pos == '' || self.y_pos == ''
      errors.add(:base, "Please choose a Direction & Grid Position")
      throw :abort
    end
  end
 
  def check_all_playing_values?
    if self.x_pos == '' || self.y_pos == ''
      errors.add(:base, "Please choose a Grid Position")
      throw :abort
    end
  end

  def put_deploying_grids
    display_put self.p1_game_grid 
  end

  def put_playing_grids
    puts "p1_display_grid"
    display_put self.p1_display_grid
    puts "p1_ocean_grid"
    display_put self.p1_ocean_grid
    puts "p1_game_grid"
    display_put self.p1_game_grid
    puts "p2_display_grid"
    display_put self.p2_display_grid
    puts "p2_ocean_grid"
    display_put self.p2_ocean_grid
    puts "p2_game_grid"
    display_put self.p2_game_grid 
  end

  def display_put(a_grid)
    puts build_display_header a_grid.size
    i = 1
    a_grid.each do |x|
      print "#{@letters[i]} "
      r = ""
      x.each do |y|
        case y
        when "@"
          r << y << " "
        when "X"  
          r << y.blue << " "
        when "H"  
          r << y.red << " "
        when "S"  
          r << y.red.bold << " "                    
        else
          r << y.green << " "
        end 
      end
      puts r
      i += 1
    end
  end

  def build_display_header(len)
    cols = "  "
    (1..len).each do |h|
      cols += "#{h} "
    end
    return cols
  end

  def fill_letters(size)
    i = 1
    ("A".."Z").each {|let|
      @letters[i] = let
      @numbers[let] = i
      i += 1
      break unless i <= size
    }
  end
end